package com.view.where2buy;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.*;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.model.where2buy.HttpHandler;
import com.model.where2buy.Product;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;

import static com.view.where2buy.AddActivity.existingProductList;

public class ListActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "ListActivity";

    private GoogleApiClient mGoogleApiClient;

    public static boolean isDisplayOnMap = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        Toolbar listToolbar = (Toolbar) findViewById(R.id.list_toolbar);
        setSupportActionBar(listToolbar);

        ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_home_black_24dp);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowTitleEnabled(false);


        // [START configure_signin]
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                //.requestScopes(new Scope(Scopes.PROFILE)
                .build();
        // [END configure_signin]

        // [START build_client]
        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        // [END build_client]

        ScrollView scroll_view = (ScrollView) findViewById(R.id.scrollViewList);

        LayoutInflater inflater = (LayoutInflater) this.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout scrollViewLinearlayout = (LinearLayout)findViewById(R.id.scroll_view_linear_layout);

        Button btnCancel = (Button) findViewById(R.id.buttonCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                redirectCancel();
            }
        });


        Button btnContinue = (Button) findViewById(R.id.buttonContinue);
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addProductAnyway();
            }
        });

        for(int i = 1; i < existingProductList.size(); i++){
            LinearLayout layout2 = new LinearLayout(this);
            layout2.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            View item = inflater.inflate(R.layout.item_layout, null, false);

            Button btnAdd = (Button) item.findViewById(R.id.btnAdd);
            TextView txtproductName = (TextView) item.findViewById(R.id.textProductName);
            TextView txtDistance = (TextView) item.findViewById(R.id.textDistance);
            TextView txtEval = (TextView) item.findViewById(R.id.textEvaluation);
            RelativeLayout relativeLayoutList = (RelativeLayout) item.findViewById(R.id.relativeLayoutList);

            final Product product = existingProductList.get(i);
            txtproductName.setText(product.getName());
            Double d = new Double(product.getEvaluation());
            txtEval.setText(String.format("%d%%",d.intValue()));
            d = new Double(product.getDistance()*1000);
            txtDistance.setText(String.format("%d m",d.intValue()));


            btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    redirectDetailProduct(product);
                }
            });

            relativeLayoutList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isDisplayOnMap = true;
                    redirectMapProduct(product);
                }
            });

            layout2.setId(i-1);
            layout2.addView(item);
            scrollViewLinearlayout.addView(layout2);
        }
    }

    // If we want an icon in the popup menu
    @Override
    protected boolean onPrepareOptionsPanel(View view, Menu menu) {
        if (menu != null) {
            if (menu.getClass().getSimpleName().equals("MenuBuilder")) {
                try {
                    Method m = menu.getClass().getDeclaredMethod(
                            "setOptionalIconsVisible", Boolean.TYPE);
                    m.setAccessible(true);
                    m.invoke(menu, true);
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName(), "onMenuOpened...unable to set icons for overflow menu", e);
                }
            }
        }
        return super.onPrepareOptionsPanel(view, menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sign_out:
                signOut();
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    // [START signOut]
    private void signOut() {
        System.out.println("signout");
        if (mGoogleApiClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            System.out.println("signout success" + status);
                            Intent intent = new Intent(ListActivity.this, LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                    | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(intent);
                            /*
                            // I don't know which one is best, if it ever doesn't work we can try this.
                            // Otherwise see http://stackoverflow.com/questions/18442328/how-to-finish-all-activities-except-the-first-activity
                            // and doc https://developer.android.com/reference/android/content/Intent.html
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                    | Intent.FLAG_ACTIVITY_CLEAR_TOP
                                    | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                            */
                        }
                    });
        }
    }
    // [END signOut]

    private void addProductAnyway() {
        existingProductList.get(0).setContext(this);
        existingProductList.get(0).add(-1, new HttpHandler.VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                try {
                    JSONObject object = new JSONObject(result);
                    JSONArray data = object.getJSONArray("0");
                    for(int i=0; i<data.length();i++) {
                        JSONObject productObject = data.getJSONObject(i);
                        System.out.println(productObject.toString());
                        System.out.println("lol" + productObject.getInt("@product_id"));
                        try{
                            if(productObject.getInt("@product_id") !=0){
                                System.out.println("lol");
                                redirectToDetailAfterAdd(productObject);
                                break;
                            }
                        }catch(Exception e){
                           Log.e("ERROR",e.toString());
                        }
                    }

                } catch (JSONException e) {
                    Log.e("ERROR",e.toString());
                    e.printStackTrace();
                }
            }
        });

    }

    public void redirectDetailProduct(Product product){
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra("Product", product);
        startActivity(intent);
    }

    public void redirectMapProduct(Product product){
        Intent intent = new Intent(this, MapsActivity.class);
        intent.putExtra("Product", product);
        startActivity(intent);
    }

    public void redirectCancel(){
        Intent intent = new Intent(this, AddActivity.class);
        startActivity(intent);
    }

    public void redirectToDetailAfterAdd(JSONObject productObject){
        try{
            existingProductList.get(0).setId(productObject.getInt("@product_id"));
            Intent intent = new Intent(this, DetailActivity.class);
            intent.putExtra("Product", existingProductList.get(0));
            startActivity(intent);
        }catch(Exception e){
            Log.e("ERROR",e.toString());
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }
}
