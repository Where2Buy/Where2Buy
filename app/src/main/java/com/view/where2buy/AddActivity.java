package com.view.where2buy;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import com.model.where2buy.HttpHandler;
import com.model.where2buy.Product;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.lang.reflect.Method;


import static com.view.where2buy.R.id.inputProduct;

public class AddActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, HttpHandler.VolleyCallback {
    private static final String TAG = "AddActivity";

    private double latitude;
    private double longitude;
    private Product product;
    public final static double DISTANCE_IN_KM = 0.05;
    public static List<Product> existingProductList;

    private GoogleApiClient mGoogleApiClient;

    private static final int TAG_CODE_PERMISSION_LOCATION = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        Toolbar addToolbar = (Toolbar) findViewById(R.id.add_toolbar);
        setSupportActionBar(addToolbar);

        ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_home_black_24dp);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowTitleEnabled(false);


        // [START configure_signin]
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                //.requestScopes(new Scope(Scopes.PROFILE)
                .build();
        // [END configure_signin]

        // [START build_client]
        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        // [END build_client]


        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, false));
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
        }
    }

    // If we want an icon in the popup menu
    @Override
    protected boolean onPrepareOptionsPanel(View view, Menu menu) {
        if (menu != null) {
            if (menu.getClass().getSimpleName().equals("MenuBuilder")) {
                try {
                    Method m = menu.getClass().getDeclaredMethod(
                            "setOptionalIconsVisible", Boolean.TYPE);
                    m.setAccessible(true);
                    m.invoke(menu, true);
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName(), "onMenuOpened...unable to set icons for overflow menu", e);
                }
            }
        }
        return super.onPrepareOptionsPanel(view, menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sign_out:
                signOut();
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    // [START signOut]
    private void signOut() {
        System.out.println("signout");
        if (mGoogleApiClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            System.out.println("signout success" + status);
                            Intent intent = new Intent(AddActivity.this, LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                    | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(intent);
                            /*
                            // I don't know which one is best, if it ever doesn't work we can try this.
                            // Otherwise see http://stackoverflow.com/questions/18442328/how-to-finish-all-activities-except-the-first-activity
                            // and doc https://developer.android.com/reference/android/content/Intent.html
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                    | Intent.FLAG_ACTIVITY_CLEAR_TOP
                                    | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                            */
                        }
                    });
        }
    }
    // [END signOut]

    public void addProductButtonOnClick(View v) {
        Log.e("latitude and longitude", "Latitude: " + latitude + ", Longitude: " + longitude);
        if(latitude != 0 && longitude != 0){
            EditText inputProduct = (EditText) findViewById(R.id.inputProduct);
            String productName = inputProduct.getText().toString();
            product = new Product(productName, longitude, latitude);
            product.setContext(this);
            product.add(DISTANCE_IN_KM,this);
        }
    }

    @Override
    public void onSuccess(String result) {
        boolean isRedirect = false;

        try {
            JSONObject object = new JSONObject(result);
            JSONArray data = object.getJSONArray("0");

            existingProductList = new ArrayList<Product>();
            existingProductList.add(product);
            for(int i=0; i<data.length();i++) {
                JSONObject productObject = data.getJSONObject(i);

                try{
                    if(productObject.getInt("@product_id") !=0){
                        product.setId(productObject.getInt("@product_id"));
                        Intent intent = new Intent(this, DetailActivity.class);
                        intent.putExtra("Product", product);
                        startActivity(intent);
                        break;
                    }
                }catch(Exception e){
                    isRedirect = true;
                    double lat = productObject.getDouble("latitude");
                    double lon = productObject.getDouble("longitude");
                    String name = productObject.getString("name");
                    double distance = productObject.getDouble("distanceFromPosition");
                    double eval = productObject.getDouble("evaluation");
                    int pId = productObject.getInt("id");
                    Product existProduct = new Product(name,lon,lat);
                    existProduct.setId(pId);
                    existProduct.setEvaluation(eval);
                    existProduct.setDistance(distance);
                    existingProductList.add(existProduct);
                }
            }

            if(isRedirect){
                Intent intent = new Intent(this, ListActivity.class);
                startActivity(intent);
            }


        } catch (JSONException e) {
            Log.e("ERROR",e.toString());
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

}
