package com.model.where2buy;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;
import com.view.where2buy.LoginActivity;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;


public class Product implements Parcelable, ClusterItem {

    private String name;
    private double longitude;
    private double latitude;
    private Context context;
    private final static String URL_PRODUCT = "/product";
    private final static String URL_EVALUATION_PRODUCT = "/evaluation_product";
    private int id;
    private double evaluation = 100;
    private LatLng mPosition;
    private double distance;


    public Product(){

    }

    public Product(String name,double longitude,double latitude){
        this.name = name;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    private Product(Parcel in) {
        name = in.readString();
        longitude = in.readDouble();
        latitude = in.readDouble();
        id = in.readInt();
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    public void add(double distance, HttpHandler.VolleyCallback callback){
        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("name", name);
        jsonParams.put("longitude", longitude+"");
        jsonParams.put("latitude", latitude+"");
        jsonParams.put("distance", distance+"");
        jsonParams.put("User_id", LoginActivity.userID);
        HttpHandler httpRequest = new HttpHandler(context, URL_PRODUCT);
        httpRequest.doPost(callback, jsonParams);


    }

    public void search(String productName, HttpHandler.VolleyCallback callback) {
        HttpHandler httpRequest = new HttpHandler(context, URL_PRODUCT+"/"+productName);
        httpRequest.doGet(callback);
    }

    public void setContext(Context context){
        this.context = context;
    }

    public String getName() {
        return name;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(double value) {
        this.evaluation = value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeDouble(longitude);
        parcel.writeDouble(latitude);
        parcel.writeInt(id);
    }

    public String fullAddress() throws IOException {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(context, Locale.getDefault());
        String fullAddress = null;

        addresses = geocoder.getFromLocation(latitude, longitude, 1);

        if(!addresses.isEmpty()) {
            System.out.println("addresses " + addresses);
            String address = addresses.get(0).getAddressLine(0);
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();
            fullAddress = address+", "+postalCode+" "+city+", "+country;
        }

        return fullAddress;
    }

    public void addEvaluation(int value,HttpHandler.VolleyCallback callback){
        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("User_id",LoginActivity.userID);
        jsonParams.put("Product_id",id+"");
        jsonParams.put("vote", value+"");
        HttpHandler httpRequest = new HttpHandler(context, URL_EVALUATION_PRODUCT);
        httpRequest.doPost(callback,jsonParams);
    }

    public void displayEvaluation(HttpHandler.VolleyCallback callback){
        if(id != 0){
            HttpHandler httpRequest = new HttpHandler(context, URL_EVALUATION_PRODUCT+"/"+id);
            httpRequest.doGet(callback);
        }else{
            Log.d("ERROR","null id");
        }
    }


    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public LatLng getPosition() {
        return new LatLng(latitude, longitude);

    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getDistance() {
        return distance;
    }
}
