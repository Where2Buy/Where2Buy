package com.model.where2buy;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.maps.android.clustering.ClusterManager;
import android.content.Context;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

public class OwnRendring extends DefaultClusterRenderer<Product> {

    private float alpha = 1.0f;

    public OwnRendring(Context context, GoogleMap map,
                       ClusterManager<Product> clusterManager) {
        super(context, map, clusterManager);
    }


    protected void onBeforeClusterItemRendered(Product item, MarkerOptions markerOptions) {
        super.onBeforeClusterItemRendered(item, markerOptions);
        //markerOptions.icon(item.getIcon());
        markerOptions.snippet(String.format("%.2f%%",item.getEvaluation()));
        markerOptions.title(item.getName());
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(146));
        markerOptions.alpha(alpha);
    }
}