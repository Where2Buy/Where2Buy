package com.model.where2buy;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;

import static java.lang.Boolean.FALSE;

public class User {

    private Context context;
    private final static String URL_USER = "/user";
    private final static String URL_INSERT_OR_UPDATE = "/insert_or_update";
    private boolean admin = FALSE;
    private boolean banned = FALSE;
    private String language = "en";
    private String name = "";
    private String givenName = "";
    private String familyName = "";
    private String email = "";
    private String id;

    public User(String name, String givenName, String familyName, String email, String id) {
        this.name = name;
        this.givenName = givenName;
        this.familyName = familyName;
        this.email = email;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void get(HttpHandler.VolleyCallback callback) {
        HttpHandler httpRequest = new HttpHandler(context, URL_USER + "/" + id);
        httpRequest.doGet(callback);
    }

    public void insertOrUpdate(HttpHandler.VolleyCallback callback) {
        Map<String, String> jsonParams = new HashMap<>();
        jsonParams.put("banned", banned ? "1" : "0");
        jsonParams.put("admin", admin ? "1" : "0");
        jsonParams.put("language", language);
        HttpHandler httpRequest = new HttpHandler(context, URL_USER + URL_INSERT_OR_UPDATE + "/" + id);
        httpRequest.doPut(callback, jsonParams);
    }
}
